# electron-quick-start

## Install dependencies

```bash
npm install .
```

## Start application

```bash
npm start .
```
