/////////////
// Imports
/////////////

const {app, BrowserWindow} = require('electron')
const path = require('path')

/////////////
// Creation
/////////////

function createWindow () {
    mainWindow = new BrowserWindow({
        kiosk: true,
        width: 1000,
        height: 1000,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        }
    })
    mainWindow.loadURL('https://public.earthcam.net/tJ90CoLmq7TzrY396Yd88P8vPBTlHsFe-r8YTXQzxxI!.tJ90CoLmq7TzrY396Yd88NEbeUJGd4aEI3VYBGXjO_E!.tJ90CoLmq7TzrY396Yd88H0yyp11FvnQcBN8TA57WfM!/5th_ave_upgrade/andy_warhol_figmentcam/live')
    mainWindow.setBackgroundColor('#000000')
    //mainWindow.webContents.openDevTools()

    // Attach main event hook
    mainWindow.webContents.once('dom-ready', () => {
        console.log("content loaded");
        setTimeout(function() {
                //mainWindow.webContents.executeJavaScript('document.getElementById("tJ90CoLmq7TzrY396Yd88H0yyp11FvnQcBN8TA57WfM!_html5_api").requestFullscreen()');
        })
    })
}


////////////////////////
// Callbacks
////////////////////////

//function

////////////////////////
// Boilerplate listeners
////////////////////////

app.whenReady().then(() => {
    createWindow()
    app.on('activate', function () {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })

})


app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
})
