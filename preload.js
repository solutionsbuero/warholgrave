// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
  const replaceText = (selector, text) => {
    const element = document.getElementById(selector)
    if (element) element.innerText = text
  }

  for (const type of ['chrome', 'node', 'electron']) {
    replaceText(`${type}-version`, process.versions[type])
  }

  upstart();
})

function upstart() {
    setTimeout(function() {
        console.log("trying it out...");
        //player = document.getElementById("tJ90CoLmq7TzrY396Yd88H0yyp11FvnQcBN8TA57WfM!_html5_api")
        //player.requestFullscreen();
        //console.log(player);
        logo = document.getElementsByClassName("Logo__Logo___19WaN");
        logo.forEach(hideElement);
        roottag = document.getElementsByTagName("html");
        roottag.forEach(setBlack);
    }, 8000);
}

function hideElement(item, index) {
    item.style.display = "none";
}

function setBlack(item, index) {
    item.style.setProperty('--player-area-background', "#000000");
}
